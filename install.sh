#!/bin/bash

# Ensure your system is up to date
sudo apt-get update

# If you do not already have it, install `pip`
sudo apt-get install python-pip

# Next, ensure that you have `python3` installed
sudo apt-get install python3

# Install the basic project requirements
pip install -r requirements.txt
