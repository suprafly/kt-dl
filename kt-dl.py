"""
kt-dl

https://gitlab.com/suprafly/kt-dl/

A simple python script to download KTs.

Requires python3.

<3
"""

import os
import argparse
import requests
import urlparse

from tqdm import tqdm
from bs4 import BeautifulSoup

def download_kt(page_url, dir):
    if is_valid_url(page_url):
        mkdir_p(dir)
        return descend(get_base_url(page_url), page_url, dir)
    else:
        print("Error: kt-dl requires a valid url")
        return False

def descend(base_url, page_url, download_dir):
    for url, ftype in get_files(base_url, page_url):
        file = url.split("&f=")[1]
        path = os.path.join(download_dir, file)
        if ftype != "directory":
            try_download_file(url, path, ftype.lower())
        else:
            mkdir_p(path)
            descend(base_url, url, download_dir)
    return True

def get_base_url(url):
    return urlparse.urljoin(url, urlparse.urlparse(url).path)

def mkdir_p(directory):
    if not dir_exists(directory):
        os.makedirs(directory)
    return directory

def is_downloaded(filename, expected_size):
    if file_exists(filename):
        has_expected_size = True if (expected_size > 0) else False
        return has_expected_size and (file_size(filename) >= expected_size)
    else:
        return False

def file_size(filename):
    return os.stat(filename).st_size

def file_exists(filename):
    return os.path.isfile(filename)

def dir_exists(directory):
    return os.path.exists(directory)

def exists(path):
    return file_exists(path) or dir_exists(path)

def convert_to_bytes(size_str):
    if "kb" in size_str:
        return to_int(size_str, "kb", 1000)
    elif "mb" in size_str:
        return to_int(size_str, "mb", 1000000)
    elif "gb" in size_str:
        return to_int(size_str, "gb", 1000000000)
    else:
        return -1

def to_int(size_str, split_str, factor):
    int(to_float(size_str, split_str) * factor)

def to_float(size_str, split_str):
    return float(size_str.split(split_str)[0])

def is_valid_url(url):
    try:
        result = urlparse.urlparse(url)
        return result.scheme and result.netloc
    except:
        return False

def get_files(base_url, page_url):
    data  = []
    for row in get_table(page_url).find_all('tr'):
        [link, ftype] = get_link_and_type(row)
        if link:
            data.append([base_url + link.get('href'), ftype])
    return data

def get_link_and_type(row):
    cols  = row.find_all('td')
    return [cols[0].find('a'), cols[1].text]

def get_table(page_url):
    resp  = requests.get(page_url)
    soup  = BeautifulSoup(resp.text, 'html.parser')
    return soup.find('table').find('table')

def try_download_file(url, filename, size_str):
    size = convert_to_bytes(size_str)
    if not is_downloaded(filename, size):
        print("Downloading: " + filename)
        resp = requests.get(url, stream=True)
        chunk_size = 1024
        total_size = int(resp.headers['content-length']) / chunk_size
        write_stream(filename, resp, chunk_size, total_size)
        print("Success!\n")
    else:
        print("Skipped (already exists): " + filename + "\n")
    return filename

def write_stream(filename, resp, chunk_size, total_size):
    with open(filename, 'wb') as f:
        for chunk in chunked_stream_progress(resp, chunk_size, total_size):
            f.write(chunk)
            f.flush()
    return True

def chunked_stream_progress(resp, chunk_size, total_size):
    return tqdm(resp.iter_content(chunk_size=chunk_size), total=total_size, unit='KB')

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Download a KT.')
    parser.add_argument('page_url', metavar='url', type=str,
                        help='a url for the fulfillment page')
    parser.add_argument('--dir', type=str, default="", help='optional dir to save files to')
    args = parser.parse_args()
    download_kt(args.page_url, args.dir)
