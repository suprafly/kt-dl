# kt-dl

A simple python script to download KTs.

* Requires python3.

## Installation

The following steps will assume you are running Ubuntu. Mac or Windows users will need to figure this out on their own.
(Feel free to submit a PR with the instructions for those OS's)

### Steps

#### Ubuntu

First, install `git`,

    $ sudo apt-get install git

Then download this repository,

    $ git clone https://gitlab.com/suprafly/kt-dl

Then `cd` into the repository directory,

    $ cd kt-dl/

Make the `install.sh` script executable,
  
    $ chmod +x install.sh

And run it to download all of the packages needed,

    $ ./install.sh

## Usage

You can simple call this script with the url of the main download link and it will download all of the files to your current directory,

    $ python kt-dl.py http://whateverurl.com/whatever

You can also specify a `--dir` option to download to a directory,

    $ python kt-dl.py http://whateverurl.com/whatever --dir kt/

## Build Executable

### Ubuntu

You can build this script into an executabe that can be run anywhere on your machine. To do this, install the `exe_requirements.txt`,

    $ pip install -r requirements_exec.txt

Then, make the `build_exec.sh` script executable,
  
    $ chmod +x build_exec.sh

Now run it,
  
    $ ./build_exec.sh

This will use the `pyinstaller` module to package the script into an executable and copy it into your `~/bin/` directory. Now you can run it from anywhere like this,

    $ kt-dl http://whateverurl.com/whatever --dir kt/
