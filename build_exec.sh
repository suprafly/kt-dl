#!/bin/bash

pyinstaller kt-dl.py --onefile \
  --exclude-module pyinstaller \
  --exclude-module altgraph \
  --exclude-module dis3 \
  --exclude-module future \
  --exclude-module macholib \
  --exclude-module pefile \
  --exclude-module click

cp dist/kt-dl ~/bin/
